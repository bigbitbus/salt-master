{% set provider_id = grains.get('provider_id','somecloud') %}
{% set testbranch = grains.get('testgitref','sometest') %}
{% set curtime = salt['cmd.run']('date +%s') %}
{% set archivename = [curtime,testbranch,provider_id]|join('__' ) %}
{% set output_dir = '/tmp/outputdata' %}

#archive.tar:
#  salt.function:
#    - tgt: '[gc,aw,az,do]*'
#     - czf 
#      - /tmp/outputdata.tar.gz 
#      - {{ output_dir }}/*
#    - kwarg:
#      - cwd: /tmp

#cp.push_dir:
#  salt.function:
#    - tgt: '[gc,aw,az,do]*'
#    - arg:
#      - {{ output_dir }}

#cmd.run:
#  salt.function:
#    - tgt: sm.novalocal
#    - arg:
#      - tar --transform 's,^\.,{{ archivename }},' -cvzf /tmp/{{ archivename }}.tar.gz .
#      - cwd=/var/cache/salt/master/minions/

