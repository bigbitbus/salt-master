# Load the mysql database, whose connection settings are in jmeter-formula/jmeter/map.jinja, 
# or more accurately in the pillar overriding that data
# Note - assumes the IP can connect up to the mysql database.

{% set loaddb_url = pillar.get('loaddb_url',"NO URL") %}
{% set loaddb_files = pillar.get('loaddb_fileNames',[]) %}
{% set mysql_user = salt['pillar.get']('dbconnection:db_username','root') %}
{% set mysql_password = salt['pillar.get']('dbconnection:db_password','password') %}
{% set mysql_host = salt['pillar.get']('dbconnection:db_host','127.0.0.1') %}
{% set mysql_port = salt['pillar.get']('dbconnection:db_port','3306') %}

all_prereqs:
  pkg.latest:
    - pkgs: 
      - mysql-client

get_db_dump:
  archive.extracted:
    - name: /tmp
    - source: {{ loaddb_url }}
    - skip_verify: True
  
{% for fileName in loaddb_files %}
import_db_file_{{ fileName }}:
  cmd.run:
    - name: mysql -u{{ mysql_user }} -p{{ mysql_password }} -h {{ mysql_host }} --port {{ mysql_port }} < {{ fileName }}
    - cwd: /tmp/test_db-master
{% endfor %}

create_indexes: #Currently only specific to employee DB
  cmd.run:
    - names:
      - mysql -u{{ mysql_user }} -p{{ mysql_password }} -h {{ mysql_host }} --port {{ mysql_port }} -Bse "USE employees; CREATE INDEX first_name ON employees(first_name)"
      - mysql -u{{ mysql_user }} -p{{ mysql_password }} -h {{ mysql_host }} --port {{ mysql_port }} -Bse "USE employees; CREATE INDEX last_name ON employees(last_name)"
    - requires: import_db_file_employees

#Potential issue - the salt logs are written with the password in them
