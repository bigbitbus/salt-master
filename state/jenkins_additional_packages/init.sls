{% set tfurl = "https://releases.hashicorp.com/terraform/0.11.8/terraform_0.11.8_linux_amd64.zip" %}
install_terraform:
  pkg.installed:
    - names:
      - unzip

  cmd.run:
    - names:
      - rm -rf terraform*
      - wget {{ tfurl }} -O terraform.zip
      - unzip -o terraform.zip -d /usr/bin
    - cwd: /tmp
    - require:
      - pkg: unzip

install_jinja2:
  pkg.installed:
    - names:
        - python-pip
        - git
        - curl

  pip.installed:
    - names:
        - requests
        - jinja2
        - jinja2-cli
        - pyyaml
    - require:
        - pkg: python-pip
