add_masterd_subdir:
  file.directory:
    - name: /etc/salt/master.d
    - makedirs: True
 
install_pygit2:
  cmd.script:
      - name: pygit2.sh
      - source: salt://masterconfig/files/pygit2-ubuntu.sh
      - cwd: /tmp

add_base_configuration:
  file.managed:
    - name: /etc/salt/master.d/base.conf
    - source: salt://masterconfig/files/base.conf
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - requires:
      - install_pygit2

# This is for states via gitfs
add_gitfs_configuration:
  file.managed:
    - name: /etc/salt/master.d/gitfs.conf
    - source: salt://masterconfig/files/gitfs.conf
    - user: root
    - group: root
    - mode: 644
    - requires:
      - install_pygit2

# This is for pillars via external git pillar
ext_pillar_git:
  file.managed:
    - name: /etc/salt/master.d/extgitpillar.conf
    - source: salt://masterconfig/files/extgitpillar.conf
    - user: root
    - group: root
    - mode: 644
    - requires:
      - install_pygit2

# This is for salt api - assumed to be installed earlier.
salt_api:
  file.managed:
    - name: /etc/salt/master.d/api.conf
    - source: salt://masterconfig/files/api.conf
    - user: root
    - group: root
    - mode: 644

