        #Pygit2 install start
if [ -e /pygit2_from_source_installed ];then
  echo "libgit2+pygit2 already installed from source - /pygit2_from_source_installed exists."
  exit 0
fi
cd /tmp
apt-get update
apt-get install -y cmake build-essential libssl-dev libffi-dev python-dev python-pip
wget https://github.com/libgit2/libgit2/archive/v0.27.0.tar.gz
tar -xvf v0.27.0.tar.gz
cd libgit2-0.27.0/
cmake .
make
make install
ldconfig
pip install pygit2
touch /pygit2_from_source_installed
#Pygit2 end