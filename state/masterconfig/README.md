# Masterconfig Salt state
The masterconfig state is used on a saltmaster (we assume that the saltmaster is also a salt minion) to:

* Setup a gitfs state backend /etc/master.d/gitfs.conf
* Setup an git-based external pillar
* Install pygit2 - this is a pre-requisite for the above.

It is assumed that the "masterconfig" state files are made available on the saltmaster via some out-of-bound mechanism. This usually means that the _masterconfig_ folder is available in the _/srv/salt_ folder and the saltmaster is using the (default) salt master configuration file.

When run the state will not restart the saltmaster process, since this is known to cause issues with a running state (masterconfig) itself being interrupted. A restart is needed, so restart the salt-master process after the state is complete.


## Usage
First, check if the masterconfig state is available at the saltmaster minion.
```
salt sm cp.list_states | grep masterconfig
```
If masterconfig is not available, check your salt master configuration, the out-of-band method used to deliver the masterconfig state, and the directory in which the masterconfig state was placed.

If the masterconfig state is available an optional pillar can be specified to override the defaults in the [Jinja map file](map.jinja). This is very useful because it be used to define the specific states that are needed as well as the pillar values consumed by these states. Look at the [pillar.example](pillar.example) to see some examples.

## Expected Results

