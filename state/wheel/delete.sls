# Only T_Client minion can execute wheel commands
{% if data['id'] == 'T_Client' %}
{% set payload = data['data'] %}
delete_key:
  wheel.key.delete:
    - args:
      - match: {{ payload['deletedminion'] }}
{% endif %}
