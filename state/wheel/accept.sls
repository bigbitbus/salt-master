# Only T_Client minion can execute wheel commands
{% if data['id'] == 'T_Client' %}
{% set payload = data['data'] %}
{% set val_testgitref = grains.get('testgitref','test_not_defined') %}
add_key:
  wheel.key.accept:
    - args:
      - match: {{ payload['acceptedminion'] }}
      
#The add_grains below doesnt work too well - for now use the runtest script to set grain.
add_grains:
  local.state.single:
    
    - tgt: '{{ payload['acceptedminion'] }}'
    - args:
      - fun: grains.present
      - name: testgitref
      - value: {{  val_testgitref }}
{% endif %}

